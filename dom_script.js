const textInput = document.getElementById('text-input');
const btnAdd = document.getElementById('btn-add');
const btnRemove = document.getElementById('btn-remove');
const content = document.getElementById('content');

function addContent() {
    const element = document.createElement("p");
    const deleteBtn = document.createElement("button")
    element.textContent = `${content.children.length + 1}. ${textInput.value}`;
    element.setAttribute("id", content.children.length);
    element.setAttribute("class", "box");
    deleteBtn.textContent = `Delete`;
    deleteBtn.setAttribute("id", content.children.length- 1);
    deleteBtn.setAttribute("class", "deleteBtn");
    deleteBtn.setAttribute("onclick", "deleteContent(this.parentNode.id)");
    content.append(element);
    element.append(deleteBtn);

    textInput.value = "";
}

function removeContent() {
    content.lastChild.remove();
}

function deleteContent(any){
    const a = document.getElementById(any);
    a.remove();
}