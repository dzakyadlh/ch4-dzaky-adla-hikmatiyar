class Animal {
    constructor(name, asal){
        this.name = name;
        this.asal = asal;
    }
    introduce() {
        console.log (`Hai hai, aku ${this.name} asalku dari ${this.asal} `);
    }
}

class Cat extends Animal {
    walking() {
        console.log (`${this.name} is walking`);
    }
    running() {
        console.log (`${this.name} is running`);
    }
    eating() {
        console.log (`${this.name} is eating`);
    }
    doAbility() {
        this.walking();
        this.running();
        this.eating();
    }
}

class Frog extends Animal {
    singing() {
        console.log (`${this.name} is singing`);
    }
    jumping() {
        console.log(`${this.name} is jumping`);
    }
    doAbility() {
        this.singing();
        this.jumping();
    }
}

let Pewpew = new Cat ('Pewpew', 'California');
let Tod = new Frog ('Tod', 'Amazon');

//seal function

const volumeBalok1 = volumeBalok(10,8,6);
const volumeBalok2 = volumeBalok(13,3,5);
const luasBalok1 = luasBalok(10,8,6);
const luasBalok2 = luasBalok(13,3,5);

console.log(volumeBalok1);
console.log(volumeBalok2);
console.log(luasBalok1);
console.log(luasBalok2);

function volumeBalok (p, l, t) {
    return p*l*t;
}

function luasBalok (p, l, t) {
    return (2*p*l)+(2*p*t)+(2*l*t);
}

//  updateAge = private

class Mouse{
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    #updateAge() {
        console.log(`updated`);
    }
    attraction() {
        console.log(`My name is ${this.name}`);
        console.log(`My age is ${this.age} years old`);
    }
}

const Jerry = new Mouse('Jerry', 10);

Jerry.updateAge();
// Error
Jerry.attraction();
// My name is jerry
// My age is 11 years old
